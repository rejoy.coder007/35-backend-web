<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function formValidation()
    {
        return view('form-validation');
    }


    public function formValidationPost(Request $request)
    {



     //   print_r($request->all());
        $this->validate($request,[
            'name' => 'required|min:2|max:50',
            'phone' => 'required|numeric',
            'email' => 'required|email',
            'password' => 'required|min:6|same:confirm_password',
            'confirm_password' => 'required|min:6|max:20',

        ],[
            'name.required' => ' The first name field is required -- .',
            'name.min' => '  Minimum required length issue It has to be more than 2 .',
            'password.required' => 'Password Required --',
            'password.same' =>  'Password not confirmed',
        ]);

        $request->session()->put('success','Form submitted properly');

        return redirect()->to('form-validation');




        /*
        $input = request()->except('password','confirm_password');
        $user=new User($input);
        $user->password=bcrypt(request()->password);
        $user->save();
        return back()->with('success', 'User created successfully.');*/

    }



}

<header>


    <div class="navigation">
        <input type="checkbox" class="navigation__checkbox" id="navi-toggle">

        <label for="navi-toggle" class="navigation__button">
            <span class="navigation__icon">&nbsp;</span>
        </label>

        <div class="navigation__background">&nbsp;</div>

        <nav class="navigation__nav">
            <ul class="navigation__list">
                <li class="navigation__item"><a href="#" class="navigation__link"><span>01</span>Item 1</a></li>
                <li class="navigation__item"><a href="#" class="navigation__link"><span>02</span>Item 2</a></li>
                <li class="navigation__item"><a href="#" class="navigation__link"><span>03</span>Item 3</a></li>
                <li class="navigation__item"><a href="#" class="navigation__link"><span>04</span>Item 4</a></li>
                <li class="navigation__item"><a href="#" class="navigation__link"><span>05</span>Item 5</a></li>
            </ul>
        </nav>
    </div>


    {{--

       <div class="top-nav container">
        <div class="logo">
            <img src="http://wayanadtoursandtravels.com/02_IMAGES/favicon.png" alt="Logo" class="header__logo">

        </div>
        <ul>
            <li><a href="#">About</a></li>
            <li><a href="#">Testimony</a></li>
            <li><a href="#">Best Products</a></li>
            <li><a href="#">New Arrival</a></li>
        </ul>
    </div> <!-- end top-nav -->

    --}}

    <div class="hero container">
        <div class="hero-copy">
            <h1>Hardware Shop</h1>
            <p>Quality Product</p>
            {{--
            <div class="hero-buttons">
                <a href="#" class="button button-white">Button 1</a>
                <a href="#" class="button button-white">Button 2</a>
            </div>--}}
        </div> <!-- end hero-copy -->

        <div class="hero-image">
            <img src="img/aa_header/1.jpg" alt="hero image">
        </div>
    </div> <!-- end hero -->










</header>
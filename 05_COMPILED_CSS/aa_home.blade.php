html {
  box-sizing: border-box; }

*, *:before, *:after {
  box-sizing: inherit; }

body {
  font-family: 'Roboto', Arial, sans-serif;
  font-size: 18px;
  font-weight: 300;
  line-height: 1.6; }

a {
  text-decoration: none;
  color: #212121; }
  a:visited {
    color: #212121; }

h1, h2 {
  font-family: 'Montserrat', Arial, sans-serif;
  font-weight: bold; }

h1 {
  font-size: 38px;
  margin-bottom: 40px; }

h2 {
  font-size: 22px;
  margin-bottom: 10px; }

img {
  max-width: 100%; }

.text-center {
  text-align: center; }

.container {
  max-width: 1200px;
  margin: auto; }

.button {
  border: 1px solid #212121;
  padding: 12px 40px; }
  .button:hover {
    color: #e9e9e9;
    background: #212121; }

.button-white {
  border: 1px solid #e9e9e9;
  color: #e9e9e9 !important; }
  .button-white:hover {
    color: #212121 !important;
    background: #e9e9e9; }

.section-description {
  width: 80%;
  margin: auto; }

.button-container {
  margin: 80px 0; }

header {
  background-image: linear-gradient(to right bottom, rgba(255, 0, 0, 0.8), rgba(40, 180, 133, 0.1)), url("/img/aa_header/aa_bg.jpg");
  background-size: cover;
  color: white;
  /*
  .top-nav
  {
    display: flex;
    justify-content: space-between;
    padding: 40px 0;

    .logo {
      font-weight: bold;
      font-size: 28px;
      margin-top: 20px;
    }

    ul {
      display: flex;
      text-transform: uppercase;
      justify-content: space-between;
      width: 50%;

      a {
        color: $white;
        &:hover {
          color: darken($white, 10%);
        }
      }
    }
  }
*/ }
  header .header__logo {
    width: 40px;
    height: 40px;
    border-radius: 50%; }

.hero {
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 30px;
  padding-top: 20px;
  padding-bottom: 84px; }
  .hero .hero-image {
    padding-left: 60px; }
  .hero h1 {
    font-size: 52px;
    margin-top: 50px; }
  .hero p {
    margin: 40px 0 68px; }
  .hero .button {
    margin-right: 14px; }

.featured-section {
  padding: 50px 0; }
  .featured-section .products {
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    grid-gap: 30px; }

.testimony-section {
  background: #F5F5F5;
  border-top: 1px solid #CDCDCD;
  padding: 50px 0; }
  .testimony-section .testimony-posts {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: 30px;
    margin: 60px 0 60px;
    grid-template-areas: "testimony2 testimony1 testimony3"; }
    .testimony-section .testimony-posts #testimony1 {
      grid-area: testimony1; }
    .testimony-section .testimony-posts #testimony2 {
      grid-area: testimony2; }
    .testimony-section .testimony-posts #testimony2 {
      grid-area: testimony3; }

footer {
  background: #535353;
  color: #e9e9e9;
  padding: 40px 0; }
  footer .footer-content {
    display: flex;
    justify-content: space-between; }
    footer .footer-content ul {
      display: flex;
      justify-content: space-between;
      width: 30%; }
      footer .footer-content ul a {
        color: #e9e9e9; }

/*==========  Non-Mobile First Method  ==========*/
/* Large Devices, Wide Screens */
@media only screen and (max-width: 1200px) {
  .container {
    max-width: 960px; } }

/* Medium Devices, Desktops */
@media only screen and (max-width: 992px) {
  header .top-nav {
    flex-direction: column; }
    header .top-nav .logo {
      margin: auto; }
    header .top-nav ul {
      margin: 20px auto 0;
      width: 100%; }
  header .hero {
    grid-template-columns: 1fr;
    text-align: center; }
    header .hero .hero-image {
      padding-left: 0;
      margin-top: 40px; }
  .featured-section {
    padding: 50px 0; }
    .featured-section .products {
      grid-template-columns: 1fr; }
  .testimony-section .testimony-posts {
    grid-template-columns: 1fr;
    text-align: center;
    grid-template-areas: "testimony1" "testimony2" "testimony3"; }
  footer .footer-content {
    flex-direction: column; }
    footer .footer-content .made-with {
      margin: auto; }
    footer .footer-content ul {
      margin: 20px auto;
      width: 90%; } }

/* Small Devices, Tablets */
/* Extra Small Devices, Phones */
/* Custom, iPhone Retina */
.navigation__checkbox {
  display: none; }

.navigation__button {
  background-color: #fff;
  height: 5rem;
  width: 5rem;
  position: fixed;
  top: 3rem;
  right: 2rem;
  border-radius: 50%;
  z-index: 2000;
  box-shadow: 0 1rem 3rem rgba(0, 0, 0, 0.1);
  text-align: center;
  cursor: pointer; }
  @media only screen and (max-width: 56.25em) {
    .navigation__button {
      top: 1rem;
      right: 1rem;
      height: 4rem;
      width: 4rem; } }
  @media only screen and (max-width: 37.5em) {
    .navigation__button {
      top: 1rem;
      right: 1rem;
      height: 4rem;
      width: 4rem; } }

.navigation__background {
  height: 4rem;
  width: 4rem;
  border-radius: 50%;
  position: fixed;
  top: 3.5rem;
  right: 2.5rem;
  z-index: 1000;
  transition: transform 0.8s cubic-bezier(0.86, 0, 0.07, 1);
  background-image: linear-gradient(to right bottom, red, rgba(40, 180, 133, 0.9));
  background-size: cover; }
  @media only screen and (max-width: 56.25em) {
    .navigation__background {
      top: 1.5rem;
      right: 1.5rem;
      height: 3rem;
      width: 3rem; } }
  @media only screen and (max-width: 37.5em) {
    .navigation__background {
      top: 1.5rem;
      right: 1.5rem;
      height: 3rem;
      width: 3rem; } }

.navigation__nav {
  height: 100vh;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 1500;
  opacity: 0;
  width: 0;
  transition: all 0.8s cubic-bezier(0.68, -0.55, 0.265, 1.55); }

.navigation__list {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  list-style: none;
  text-align: center;
  width: 100%; }

.navigation__item {
  margin: 1rem; }

.navigation__link:link, .navigation__link:visited {
  display: inline-block;
  font-size: 2rem;
  font-weight: 300;
  padding: 1rem 2rem;
  color: #fff;
  text-decoration: none;
  text-transform: uppercase;
  background-image: linear-gradient(120deg, transparent 0%, transparent 50%, #fff 50%);
  background-size: 220%;
  transition: all .4s; }
  .navigation__link:link span, .navigation__link:visited span {
    margin-right: 1.5rem;
    display: inline-block; }

.navigation__link:hover, .navigation__link:active {
  background-position: 100%;
  color: #ff66cc;
  transform: translateX(1rem); }

.navigation__checkbox:checked ~ .navigation__background {
  transform: scale(80); }

.navigation__checkbox:checked ~ .navigation__nav {
  opacity: 1;
  width: 100%; }

.navigation__icon {
  position: relative;
  margin-top: 2.5rem; }
  @media only screen and (max-width: 56.25em) {
    .navigation__icon {
      margin-top: 2.0rem; } }
  @media only screen and (max-width: 37.5em) {
    .navigation__icon {
      margin-top: 2.0rem; } }
  .navigation__icon, .navigation__icon::before, .navigation__icon::after {
    width: 3rem;
    height: 2px;
    background-color: #333;
    display: inline-block; }
  .navigation__icon::before, .navigation__icon::after {
    content: "";
    position: absolute;
    left: 0;
    transition: all .2s; }
  .navigation__icon::before {
    top: -.8rem; }
  .navigation__icon::after {
    top: .8rem; }

.navigation__button:hover .navigation__icon::before {
  top: -1rem; }

.navigation__button:hover .navigation__icon::after {
  top: 1rem; }

.navigation__checkbox:checked + .navigation__button .navigation__icon {
  background-color: transparent; }

.navigation__checkbox:checked + .navigation__button .navigation__icon::before {
  top: 0;
  transform: rotate(135deg); }

.navigation__checkbox:checked + .navigation__button .navigation__icon::after {
  top: 0;
  transform: rotate(-135deg); }
